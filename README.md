# Spring Boot Angular

This is a simple application that make use of the iTunes API to demonstrate how to use Spring Boot with AngularJS.  Other technologies will be used in this example like maven, bower, grunt, testng, etc.  The initial application will only have an index.html page to load just to prove out Spring Boot is working.

## Running Spring Boot Angular

1. Open terminal window

2. Move to project directory 'cd %project_home%'

3. Run 'mvn package'

4. Run the app 'java -jar target/spring-boot-angular-0.0.6-SNAPSHOT.war'

