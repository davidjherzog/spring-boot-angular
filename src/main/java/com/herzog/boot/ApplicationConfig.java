package com.herzog.boot;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author dherzog
 * 
 * Application Configuration
 *
 */
@Configuration
public class ApplicationConfig {

	/**
	 * Create RestTemplate bean
	 * 
	 * @return RestTemplate
	 */
	@Bean
	public RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();

		return restTemplate;
	}

}
