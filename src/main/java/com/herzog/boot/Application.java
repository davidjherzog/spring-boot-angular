package com.herzog.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author dherzog
 * 
 * Application
 *
 */
@ComponentScan
@EnableAutoConfiguration
public class Application {
	
	/**
	 * Application starting point
	 * 
	 * @param args String[]
	 */
	public static void main(final String[] args) {
		SpringApplication.run(Application.class, args);
	}
}