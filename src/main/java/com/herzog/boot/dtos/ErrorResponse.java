package com.herzog.boot.dtos;

import java.io.Serializable;

/**
 * @author dherzog
 *
 * Error Response
 */
public class ErrorResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

	@Override
	public String toString() {
		return String.format("ErrorResponse [message=%s]", message);
	}
}

