package com.herzog.boot.dtos;

import java.io.Serializable;
import java.util.List;
import com.herzog.boot.dtos.itunes.Type;

/**
 * @author dherzog
 * 
 * Album Response
 *
 */
public class AlbumResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int resultCount;
	private List<Type> results;
	
	public int getResultCount() {
		return resultCount;
	}
	
	public void setResultCount(final int resultCount) {
		this.resultCount = resultCount;
	}
	
	public List<Type> getResults() {
		return results;
	}
	
	public void setResults(final List<Type> results) {
		this.results = results;
	}

	@Override
	public String toString() {
		return String.format("AlbumResponse [resultCount=%s, results=%s]", resultCount, results);
	}

}
