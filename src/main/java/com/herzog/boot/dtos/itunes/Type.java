package com.herzog.boot.dtos.itunes;

import java.io.Serializable;

/**
 * @author dherzog
 * 
 * iTunes Type DTO
 *
 */
public class Type implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String wrapperType;
	private String kind;
	private String collectionType;
	private int artistId;
	private int collectionId;
	private int trackId;
	private int amgArtistId;
	private String artistName;
	private String collectionName;
	private String trackName;
	private String collectionCensorName;
	private String trackCensorName;
	private String artistViewUrl;
	private String collectionViewUrl;
	private String trackViewUrl;
	private String previewUrl;
	private String artworkUrl30;
	private String artworkUrl60;
	private String artworkUrl100;
	private double collectionPrice;
	private double trackPrice;
	private String collectionExplicitness;
	private String trackExplicitness;
	private int discCount;
	private int discNumber;
	private int trackCount;
	private int trackNumber;
	private int trackTimeMillis;
	private String copyright;
	private String country;
	private String currency;
	private String releaseDate;
	private String primaryGenreName;
	private String radioStationUrl;
	
	public String getWrapperType() {
		return wrapperType;
	}
	
	public void setWrapperType(final String wrapperType) {
		this.wrapperType = wrapperType;
	}
	
	public String getKind() {
		return kind;
	}
	
	public void setKind(final String kind) {
		this.kind = kind;
	}
	
	public String getCollectionType() {
		return collectionType;
	}
	
	public void setCollectionType(final String collectionType) {
		this.collectionType = collectionType;
	}
	
	public int getArtistId() {
		return artistId;
	}
	
	public void setArtistId(final int artistId) {
		this.artistId = artistId;
	}
	
	public int getCollectionId() {
		return collectionId;
	}
	
	public void setCollectionId(final int collectionId) {
		this.collectionId = collectionId;
	}
	
	public int getTrackId() {
		return trackId;
	}
	
	public void setTrackId(final int trackId) {
		this.trackId = trackId;
	}
	
	public int getAmgArtistId() {
		return amgArtistId;
	}
	
	public void setAmgArtistId(final int amgArtistId) {
		this.amgArtistId = amgArtistId;
	}
	
	public String getArtistName() {
		return artistName;
	}
	
	public void setArtistName(final String artistName) {
		this.artistName = artistName;
	}
	
	public String getCollectionName() {
		return collectionName;
	}
	
	public void setCollectionName(final String collectionName) {
		this.collectionName = collectionName;
	}
	
	public String getTrackName() {
		return trackName;
	}
	
	public void setTrackName(final String trackName) {
		this.trackName = trackName;
	}
	
	public String getCollectionCensorName() {
		return collectionCensorName;
	}
	
	public void setCollectionCensorName(final String collectionCensorName) {
		this.collectionCensorName = collectionCensorName;
	}
	
	public String getTrackCensorName() {
		return trackCensorName;
	}
	
	public void setTrackCensorName(final String trackCensorName) {
		this.trackCensorName = trackCensorName;
	}
	
	public String getArtistViewUrl() {
		return artistViewUrl;
	}
	
	public void setArtistViewUrl(final String artistViewUrl) {
		this.artistViewUrl = artistViewUrl;
	}
	
	public String getCollectionViewUrl() {
		return collectionViewUrl;
	}
	
	public void setCollectionViewUrl(final String collectionViewUrl) {
		this.collectionViewUrl = collectionViewUrl;
	}
	
	public String getTrackViewUrl() {
		return trackViewUrl;
	}
	
	public void setTrackViewUrl(final String trackViewUrl) {
		this.trackViewUrl = trackViewUrl;
	}
	
	public String getPreviewUrl() {
		return previewUrl;
	}
	
	public void setPreviewUrl(final String previewUrl) {
		this.previewUrl = previewUrl;
	}
	
	public String getArtworkUrl30() {
		return artworkUrl30;
	}
	
	public void setArtworkUrl30(final String artworkUrl30) {
		this.artworkUrl30 = artworkUrl30;
	}
	
	public String getArtworkUrl60() {
		return artworkUrl60;
	}
	
	public void setArtworkUrl60(final String artworkUrl60) {
		this.artworkUrl60 = artworkUrl60;
	}
	
	public String getArtworkUrl100() {
		return artworkUrl100;
	}
	
	public void setArtworkUrl100(final String artworkUrl100) {
		this.artworkUrl100 = artworkUrl100;
	}
	
	public double getCollectionPrice() {
		return collectionPrice;
	}
	
	public void setCollectionPrice(final double collectionPrice) {
		this.collectionPrice = collectionPrice;
	}
	
	public double getTrackPrice() {
		return trackPrice;
	}
	
	public void setTrackPrice(final double trackPrice) {
		this.trackPrice = trackPrice;
	}
	
	public String getCollectionExplicitness() {
		return collectionExplicitness;
	}
	
	public void setCollectionExplicitness(final String collectionExplicitness) {
		this.collectionExplicitness = collectionExplicitness;
	}
	
	public String getTrackExplicitness() {
		return trackExplicitness;
	}
	
	public void setTrackExplicitness(final String trackExplicitness) {
		this.trackExplicitness = trackExplicitness;
	}
	
	public int getDiscCount() {
		return discCount;
	}
	
	public void setDiscCount(final int discCount) {
		this.discCount = discCount;
	}
	
	public int getDiscNumber() {
		return discNumber;
	}
	
	public void setDiscNumber(final int discNumber) {
		this.discNumber = discNumber;
	}
	
	public int getTrackCount() {
		return trackCount;
	}
	
	public void setTrackCount(final int trackCount) {
		this.trackCount = trackCount;
	}
	
	public int getTrackNumber() {
		return trackNumber;
	}
	
	public void setTrackNumber(final int trackNumber) {
		this.trackNumber = trackNumber;
	}
	
	public int getTrackTimeMillis() {
		return trackTimeMillis;
	}
	
	public void setTrackTimeMillis(final int trackTimeMillis) {
		this.trackTimeMillis = trackTimeMillis;
	}
	
	public String getCopyright() {
		return copyright;
	}
	
	public void setCopyright(final String copyright) {
		this.copyright = copyright;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(final String country) {
		this.country = country;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(final String currency) {
		this.currency = currency;
	}
	
	public String getReleaseDate() {
		return releaseDate;
	}
	
	public void setReleaseDate(final String releaseDate) {
		this.releaseDate = releaseDate;
	}
	
	public String getPrimaryGenreName() {
		return primaryGenreName;
	}
	
	public void setPrimaryGenreName(final String primaryGenreName) {
		this.primaryGenreName = primaryGenreName;
	}
	
	public String getRadioStationUrl() {
		return radioStationUrl;
	}
	
	public void setRadioStationUrl(final String radioStationUrl) {
		this.radioStationUrl = radioStationUrl;
	}

	@Override
	public String toString() {
		return String
				.format("Type [wrapperType=%s, kind=%s, collectionType=%s, artistId=%s, collectionId=%s, trackId=%s, "
						+ "amgArtistId=%s, artistName=%s, collectionName=%s, trackName=%s, collectionCensorName=%s, "
						+ "trackCensorName=%s, artistViewUrl=%s, collectionViewUrl=%s, trackViewUrl=%s, previewUrl=%s, "
						+ "artworkUrl30=%s, artworkUrl60=%s, artworkUrl100=%s, collectionPrice=%s, trackPrice=%s, "
						+ "collectionExplicitness=%s, trackExplicitness=%s, discCount=%s, discNumber=%s, "
						+ "trackCount=%s, trackNumber=%s, trackTimeMillis=%s, copyright=%s, country=%s, currency=%s, "
						+ "releaseDate=%s, primaryGenreName=%s, radioStationUrl=%s]",
						wrapperType, kind, collectionType, artistId, collectionId, trackId, amgArtistId, artistName,
						collectionName, trackName, collectionCensorName, trackCensorName, artistViewUrl,
						collectionViewUrl, trackViewUrl, previewUrl, artworkUrl30, artworkUrl60, artworkUrl100,
						collectionPrice, trackPrice, collectionExplicitness, trackExplicitness, discCount, discNumber,
						trackCount, trackNumber, trackTimeMillis, copyright, country, currency, releaseDate,
						primaryGenreName, radioStationUrl);
	}
	
	

}
