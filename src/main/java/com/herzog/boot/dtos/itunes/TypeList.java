package com.herzog.boot.dtos.itunes;

import java.io.Serializable;
import java.util.List;

/**
 * @author dherzog
 * 
 * iTunes Type List DTO
 *
 */
public class TypeList implements Serializable {

	private static final long serialVersionUID = 1L;

	private int resultCount;
	private List<Type> results;
	
	public int getResultCount() {
		return resultCount;
	}
	
	public void setResultCount(final int resultCount) {
		this.resultCount = resultCount;
	}
	
	public List<Type> getResults() {
		return results;
	}
	
	public void setResults(final List<Type> results) {
		this.results = results;
	}

	@Override
	public String toString() {
		return String.format("TypeList [resultCount=%s, results=%s]", resultCount, results);
	}
	
}
