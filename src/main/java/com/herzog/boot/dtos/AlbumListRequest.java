package com.herzog.boot.dtos;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author dherzog
 * 
 * Album List Request
 *
 */
public class AlbumListRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String artist;

	@NotNull
	@Size(min = 1, max = 100)
	public String getArtist() {
		return artist;
	}

	public void setArtist(final String artist) {
		this.artist = artist;
	}

	@Override
	public String toString() {
		return String.format("AlbumListRequest [artist=%s]", artist);
	}		

}
