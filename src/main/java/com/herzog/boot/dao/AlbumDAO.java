package com.herzog.boot.dao;

import com.herzog.boot.dtos.itunes.TypeList;

/**
 * @author dherzog
 * 
 * Album DAO
 *
 */
public interface AlbumDAO {
	
	/**
	 * Return a list of albums by a specific artist
	 * 
	 * @param artist String
	 * @return TypeList
	 * @throws Exception - throw all exceptions
	 */
	public TypeList list(final String artist) throws Exception;
	
	/**
	 * Return the properties of a specific album
	 * 
	 * @param id - Album ID
	 * @return TypeList
	 * @throws Exception - throw all exceptions
	 */
	public TypeList find(final int id) throws Exception;

}
