package com.herzog.boot.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.herzog.boot.dao.itunes.ITunesRestImpl;
import com.herzog.boot.dtos.itunes.TypeList;

/**
 * @author dherzog
 * 
 * Album DAO Implementation
 *
 */
@Component("albumDAO")
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlbumDAOImpl extends ITunesRestImpl implements AlbumDAO {

	@Override
	public TypeList list(final String artist) throws Exception {
		String url = "/search?term=" + artist + "&entity=album"; 
		return iTunesApi(url, HttpMethod.GET, null, null);
	}

	@Override
	public TypeList find(final int id) throws Exception {
		String url = "/lookup?id=" + id + "&entity=song"; 
		return iTunesApi(url, HttpMethod.GET, null, null);
	}

}
