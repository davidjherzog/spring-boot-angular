package com.herzog.boot.dao.itunes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import com.herzog.boot.dtos.itunes.TypeList;
import com.herzog.boot.utils.CustomObjectMapper;

/**
 * @author dherzog
 * 
 * iTunes REST Implementation
 */
public abstract class ITunesRestImpl {
	
	// TODO - should externalize this to properties file
	private String iTunesUrl = "https://itunes.apple.com"; 
	
	@Autowired
	protected RestTemplate restTemplate;
	
	/**
	 * Makes a REST call to iTunes
	 * 
	 * @param url - url of the resource
	 * @param method - Http method
	 * @param uriVariables - url substitution variables
	 * @return iTunes response
	 * @throws Exception an exception
	 */
	protected TypeList iTunesApi(final String url, final HttpMethod method, final Object[] uriVariables)
			throws Exception {
		return iTunesApi(url, method, null, uriVariables);
	}
	
	/**
	 * Makes a REST call to iTunes
	 * 
	 * @param url - url of the resource
	 * @param method - Http method
	 * @param body - body of the REST request
	 * @param uriVariables - url substitution variables
	 * @return iTunes response
	 * @throws Exception an exception
	 */
	protected TypeList iTunesApi(final String url, final HttpMethod method, final Object body,
			final Object[] uriVariables) throws Exception {
		ResponseEntity<String> response;
		if (uriVariables == null) {
			response = restTemplate.exchange(iTunesUrl + url, method, constructRequest(body), 
					String.class);
		} else {
			response = restTemplate.exchange(iTunesUrl + url, method, constructRequest(body), 
					String.class, uriVariables);
		}
		
		CustomObjectMapper mapper = new CustomObjectMapper();
		TypeList typeList = mapper.readValue(response.getBody(), TypeList.class);
		
		return typeList;
	}
	
	
	/**
	 * Construct request
	 * 
	 * @return HttpEntity<Object>
	 */
	public HttpEntity<?> constructRequest() {
		
		HttpEntity<?> requestEntity = new HttpEntity<Object>(constructHeaders());
		
		return requestEntity;
	}
	
	/**
	 * Construct request
	 * 
	 * @param body - Request Body
	 * @return HttpEntity<Object>
	 */
	public HttpEntity<?> constructRequest(final Object body) {
		
		HttpEntity<?> requestEntity = new HttpEntity<Object>(body, constructHeaders());
		
		return requestEntity;
	}

	private MultiValueMap<String, String> constructHeaders() {
		MultiValueMap<String, String> headers =
				new LinkedMultiValueMap<String, String>();
		headers.add("Content-type", "application/json");
		headers.add("Accept", "application/json");
		return headers;
	}

}
