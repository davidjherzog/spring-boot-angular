package com.herzog.boot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.herzog.boot.dao.AlbumDAO;
import com.herzog.boot.dtos.AlbumListRequest;
import com.herzog.boot.dtos.AlbumListResponse;
import com.herzog.boot.dtos.AlbumResponse;
import com.herzog.boot.dtos.itunes.TypeList;

/**
 * @author dherzog
 *
 * Album Service
 */
@Service("albumService")
public class AlbumServiceImpl implements AlbumService {
	
	@Autowired
	private AlbumDAO albumDAO;

	@Override
	public AlbumListResponse list(final AlbumListRequest request) throws Exception {
		TypeList typeList = albumDAO.list(request.getArtist());
		
		AlbumListResponse response = new AlbumListResponse();
		response.setResultCount(typeList.getResultCount());
		response.setResults(typeList.getResults());

		return response;
	}

	@Override
	public AlbumResponse find(final int id) throws Exception {
		
		TypeList typeList = albumDAO.find(id);
		
		AlbumResponse response = new AlbumResponse();
		response.setResultCount(typeList.getResultCount());
		response.setResults(typeList.getResults());
		
		return response;
	}

}
