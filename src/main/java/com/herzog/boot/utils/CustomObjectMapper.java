package com.herzog.boot.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;


/**
 * @author dherzog
 *
 * Custom Object Mapper
 */
public class CustomObjectMapper extends ObjectMapper {
	
	private static final long serialVersionUID = 1L;

    /**
     * Default Constructor
     *
     * Sets FAIL_ON_UNKNOWN_PROPERTIES and FAIL_ON_EMPTY_BEANS to false
     */
    public CustomObjectMapper() {
        super();
        this.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        this.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    }

}
