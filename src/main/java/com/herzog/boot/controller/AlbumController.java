package com.herzog.boot.controller;

import com.herzog.boot.dtos.AlbumListRequest;
import com.herzog.boot.dtos.AlbumListResponse;
import com.herzog.boot.dtos.AlbumResponse;

/**
 * @author dherzog
 * 
 * Album Controller
 *
 */
public interface AlbumController {
	
	/**
	 * Return a list of albums by a specific artist
	 * 
	 * @param request AlbumListRequest
	 * @return AlbumListResponse
	 * @throws Exception - throw all exceptions
	 */
	public AlbumListResponse list(final AlbumListRequest request) throws Exception;
	
	/**
	 * Return the properties of a specific album
	 * 
	 * @param id - Album ID
	 * @return AlbumResponse
	 * @throws Exception - throw all exceptions
	 */
	public AlbumResponse find(final int id) throws Exception;


}
