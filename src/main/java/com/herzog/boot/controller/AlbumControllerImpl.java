package com.herzog.boot.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.herzog.boot.dtos.AlbumListRequest;
import com.herzog.boot.dtos.AlbumListResponse;
import com.herzog.boot.dtos.AlbumResponse;
import com.herzog.boot.service.AlbumService;

/**
 * @author dherzog
 *
 * Album Controller Implementation
 */
@Controller("albumController")
@RequestMapping(value = "/api/album")
public class AlbumControllerImpl implements AlbumController {
	
	@Autowired
	private AlbumService albumService;
	
	@Override
    @RequestMapping(value = "/list", method = RequestMethod.POST, consumes = "application/json", 
    		produces = "application/json")
    @ResponseBody
	public AlbumListResponse list(@Valid @RequestBody final AlbumListRequest request) throws Exception {
		
		AlbumListResponse response = albumService.list(request);
        return response;
    }

	@Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
	public AlbumResponse find(@PathVariable(value = "id") final int id) throws Exception {
		
		AlbumResponse response = albumService.find(id);
		return response;
	}

}
