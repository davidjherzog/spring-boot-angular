package com.herzog.boot.aspect;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.herzog.boot.dtos.ErrorResponse;

/**
 * @author dherzog
 *
 * Error Handler Aspect
 */
@ControllerAdvice
public class ErrorHandlerAspect {
	
	/**
     * Method that catches java validation errors
     *
     * @param ve - Method Argument Not Valid Exception being thrown
     * @return - ErrorResponse
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
	public ErrorResponse validationError(final MethodArgumentNotValidException ve) {

    	ErrorResponse response = new ErrorResponse();

        BindingResult result = ve.getBindingResult();
        List<ObjectError> objectErrors = result.getAllErrors();
        ObjectError objectError = objectErrors.get(0);
        
        response.setMessage(objectError.getDefaultMessage());
     
        return response;
    }
    
    /**
     * Method that catches non application errors
     *
     * @param e - Exception being thrown
     * @return - ErrorResponse
     */
    @ExceptionHandler(Throwable.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorResponse serverError(final Throwable e) {
        e.printStackTrace();
        
        ErrorResponse response = new ErrorResponse();
    	response.setMessage("General Service exception - please contact customer support");
        
        return response;
    }

}
