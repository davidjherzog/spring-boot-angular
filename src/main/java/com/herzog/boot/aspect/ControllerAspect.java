package com.herzog.boot.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * @author dherzog
 *
 * Controller Aspect for all public controller methods.
 */
@Aspect
@Configurable
public class ControllerAspect {
	
	/**
     * Proxy method to log before and after service method calls.
     *
     * @param joinPoint ProceedingJoinPoint
     * @return object
     * @throws Throwable throw exception
     */
	@Around("execution(public * com.herzog.boot.controller.*ControllerImpl.*(..))")
    public final Object logAround(final ProceedingJoinPoint joinPoint) throws Throwable {

        Object request = null;
        Object response = null;

        try {
            Object[] params = joinPoint.getArgs();
            request = params[0];

            response = joinPoint.proceed(params);
            System.out.println("\n\n\t\t"
                    + "The class " + joinPoint.getSignature().getDeclaringTypeName()
                    + " with the method " + joinPoint.getSignature().getName()
                    + "()"
                    + " \n\t\t\t begins with " + params[0]
                    + " \n\t\t\t ends  with " + response + "\n");
        } catch (Exception e) {
        	Object[] params = joinPoint.getArgs();
        	System.out.println("\n\n\t\t"
                    + "Class " + joinPoint.getSignature().getDeclaringTypeName()
                    + " with the method " + joinPoint.getSignature().getName()
                    + "()"
                    + " \n\t\t\t begins with " + params[0]
                    + " \n\t\t\t and failed with the exception " + e + "\n");
            throw e;
        } 
        
        return response;
    }	

}
