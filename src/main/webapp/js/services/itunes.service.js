(function() {
  'use strict';

  angular
    .module('springBootApp')
    .factory('iTunesService', iTunesService);

  	iTunesService.$inject = ['restService'];

    function iTunesService(restService) {

      var itunes = {
        list: list,
        find: find
      };
      return itunes;

      function list(artist) {
        var postData = {
        	artist: artist
        };
        return restService.post('/api/album/list', postData);
      }
      
      function find(id) {
      	var params = {
          id: id
        };
        return restService.get('/api/album/:id', params);
      }

    }

})();