(function() {
  'use strict';

  angular
    .module('springBootApp')
    .factory('restService', restResource);

  	restResource.$inject = ['$resource'];

    function restResource($resource) {

      var resource ={
        get: get,
        post: post
      };
      return resource;

      function get(url, params) {
        
        var resource = $resource(url, {}, {
          retrieve: {
            method: 'GET',
            params: params,
            isArray: false,
            headers: {
            	'Accept': 'application/json'
            }
          }
        });
        return resource.retrieve();

      }
      
      function post(url, postData) {
        
        var resource = $resource(url, {}, {
          post: {
            method: 'POST',
            isArray: false,
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          }
        });
        return resource.post({}, postData);

      }

    }

})();