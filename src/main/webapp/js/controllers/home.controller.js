(function() {
  'use strict';

  angular
    .module('springBootApp')
    .controller('HomeController', HomeController);
  
	  HomeController.$inject = ['toastr', 'iTunesService'];
	
	  function HomeController(toastr, iTunesService) {
	    var vm = this;
	    vm.home = "Spring Boot Angular";
	    
	    vm.list = list;
	    vm.find = find;
	    	
	    function list() {
	    	iTunesService.list(vm.search).$promise.then(listSuccess, listError);
	    }
	    
	    function listSuccess(result) {
	    	vm.albums = result;
	    	vm.albumTracks = null;
	    	vm.albumInfo = null;
	    }
	    
	    function listError(result) {
	    	toastr.error(result.status + " - " + result.data.message, 'Failed loading Albums');
	    }
	    
	    function find(id) {
	    	iTunesService.find(id).$promise.then(findSuccess, findError);
	    }
	    
	    function findSuccess(result) {
	    	vm.albumTracks = result.results;
	    	vm.albumTracks.shift();
	    	vm.albumInfo = result.results[0];
	    }
	    
	    function findError(result) {
	    	toastr.error(result.status + " - " + result.data.message, 'Failed loading Album');
	    }
	    
	  }

})();
