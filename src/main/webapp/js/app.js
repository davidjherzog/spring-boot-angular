(function() {
  'use strict';

  angular.module('springBootApp', ['ui.router', 'ngResource', 'toastr']);

})();
