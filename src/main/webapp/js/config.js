(function() {
  'use strict';

  angular
    .module('springBootApp')
    .config(config);

  config.$inject = ['$stateProvider', '$urlRouterProvider'];
  function config($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('home', {
            url:'/',
            templateUrl: 'layout/home.html',
            controller: 'HomeController',
            controllerAs: 'vm'
        });
  }

})();
